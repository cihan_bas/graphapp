import React, { PureComponent } from 'react';
import { Text, View, SafeAreaView, Dimensions, StyleSheet, Animated, TextInput } from 'react-native';
import Axios from 'axios';
import Svg, {
    Path,
    Defs,
    LinearGradient,
    Stop,
} from 'react-native-svg';
import { ListItem } from "react-native-elements";
import * as dshape from 'd3-shape';
import * as d3Array from 'd3-array';
const path = require("svg-path-properties");
const { width, height } = Dimensions.get('window');
import { scaleTime, scaleLinear, scaleQuantile, scaleQuantize, scaleThreshold } from 'd3-scale';
const data = []
const maxDate = 2200
for (let i = 1992; i < maxDate; i++) {
    const obj = {
        year: i,
        value: Math.floor(Math.random() * 100)
    }
    data.push(obj)
}
console.log(data)
const colors = {
    primary: '#0F53FF',

}



const graphWidth = data.length > 20 ? width * data.length / 20 : width
const scaleX = scaleTime()
    .domain([1990, maxDate])
    .range([0, graphWidth]);
const scaleY = scaleLinear()
    .domain([100, 0])
    .range([0, 200]);
// const line = dshape.line().x(x).y(y)(data)
const line = dshape
    .line()
    .x(d => scaleX(d.year))
    .y(d => scaleY(d.value)).curve(dshape.curveNatural)
    (data);
//const scaleLabel = scaleQuantile().domain(labelDomain).range([0, 100]);

const properties = new path.svgPathProperties(line);
 const parts = properties.getParts();
let newTotal = 0
const partsArray = parts.map(item => {
    newTotal += item.length
    return newTotal
})
 const cursorRadius = 7;
const labelWidth = 100;
const partsTotal = parts.map(item => item.length)

export default class lesson1 extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            point: properties.getPointAtLength(0)
        }
        this.cursor = React.createRef();
        this.x = new Animated.Value(0);
        this.label = React.createRef();
        this.labelWrapper = React.createRef();


    }

    async componentDidMount() {
        this.x.addListener(({ value }) => this.moveCursor(value));

        this.moveCursor(0)
        // const data=await fetch("https://people.sc.fsu.edu/~jburkardt/data/csv/deniro.csv");
        // const { data } = await Axios.get("https://people.sc.fsu.edu/~jburkardt/data/csv/deniro.csv");
    }
    moveCursor(value2) {
         const transactionIndex = parseInt((value2) / 70);
        try {
            if (transactionIndex) {
                 if (transactionIndex <= data.length) {
                    const newValue2 = value2 % 70 / 70 * partsTotal[transactionIndex]
                    this.label.current.setNativeProps({ text: `${data[transactionIndex] ? (data[transactionIndex].year + ' ' + data[transactionIndex].value) : 0} EUR` });
                    const { x, y } = properties.getPointAtLength(partsArray[transactionIndex - 1] + newValue2);

                    this.graphScroll.getNode().scrollTo({ x: x - width + labelWidth, y: 100, animated: false });
                    this.cursor.current.setNativeProps({ top: y - cursorRadius, left: x - cursorRadius });
                    this.labelWrapper.current.setNativeProps({ style: { left: x - labelWidth / 2, position: `absolute` } });


                }
            } else {
                const newValue2 = value2 % 70 / 70 * partsTotal[transactionIndex]
                this.label.current.setNativeProps({ text: `${data[transactionIndex] ? (data[transactionIndex].year + ' ' + data[transactionIndex].value) : 0} EUR` });
                const { x, y } = properties.getPointAtLength(newValue2);

                this.graphScroll.getNode().scrollTo({ x: x - width + labelWidth, y: 100, animated: false });
                this.cursor.current.setNativeProps({ top: y - cursorRadius, left: x - cursorRadius });
                this.labelWrapper.current.setNativeProps({ style: { left: x - labelWidth / 2, position: `absolute` } });
            }

        } catch (error) {
            console.log('error ', error)
        }


    }

    render() {

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Animated.ScrollView horizontal={true} style={{
                    height: 300, flexGrow: 1, marginBottom: 20,
                    marginTop:20
                }}
                    ref={(ref) => {
                        this.graphScroll = ref;
                    }}

                    scrollEnabled={false}
                >
                    <Svg height={200} width={graphWidth}>
                        <Defs height={200} width={graphWidth}>
                            <LinearGradient id="gradient" x1="0%"
                                x2="0%"
                                y1="0%"
                                y2="100%"
                            >
                                <Stop offset="0%" stopColor={colors.primary} stopOpacity=".8" />
                                <Stop offset="25%" stopColor={colors.primary} stopOpacity=".25" />
                                <Stop offset="100%" stopColor={colors.primary} stopOpacity="0" />
                            </LinearGradient>
                        </Defs>
                        <Path
                            fill="url(#gradient)"
                            d={`${line}L ${graphWidth} ${200} L 0 ${200}`}
                            stroke="#367be2"
                            strokeWidth="2"

                        />
                        <View ref={this.cursor} style={styles.cursor} />

                    </Svg>
                    <Animated.View style={[styles.label,]} ref={this.labelWrapper} >
                        <TextInput ref={this.label} />
                    </Animated.View>
                </Animated.ScrollView >


                <Animated.ScrollView style={{
                    flexGrow: 1, backgroundColor: '#fff',
                }}
                    contentContainerStyle={{ paddingBottom: height - 400, }}
                    onScroll={this.handleScroll}
                    bounces={false}
                    scrollEventThrottle={1} // <-- Use 1 here to make sure no events are ever missed
                    onScroll={Animated.event(
                        [{ nativeEvent: { contentOffset: { y: this.x } } }],
                        { useNativeDriver: true } // <-- Add this
                    )}
                    disableScrollViewPanResponder={false}
                >
                    {
                        data.map((l, i) => (
                            <ListItem
                                key={i}
                                title={(l.year.toString())}
                                subtitle={l.value.toString()}
                                bottomDivider
                                style={{ height: 70 }}
                            />
                        ))
                    }
                </Animated.ScrollView>
            </SafeAreaView>
        );
    }
}
const verticalPadding = 5;
const styles = StyleSheet.create({
    root: {
        flex: 1,
    },
    container: {
        marginTop: 60,
        height,
        width,
    },
    cursor: {
        width: cursorRadius * 2,
        height: cursorRadius * 2,
        borderRadius: cursorRadius,
        borderColor: colors.primary,
        borderWidth: 2,
        backgroundColor: 'white',
    },
    label: {
        position: 'absolute',
        top: 0,
        left: 0,
        height: 30,
        zIndex: 4,
        backgroundColor: '#fff',
        width: labelWidth,
        justifyContent: `center`,
        padding: 5,
        borderRadius: 8
    },
});
