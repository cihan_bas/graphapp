/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App2 from './App';
import App from './Components/D3Demo/lesson1';
import { name as appName } from './app.json';


AppRegistry.registerComponent(appName, () => App);
